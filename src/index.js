import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import _ from 'lodash';

//component
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
//youtube api key
const API_KEY = 'AIzaSyCXKW14QlGJl-cY35fMuAIZcV8u31XCU9w';


class App extends Component {
	
	constructor (props) {
		super(props);

		this.state = {
			videos: [],
			selectedVideo: null,
		};

		this.videoSearch('surfboards');
	}

	videoSearch(term) {
		YTSearch( {key:API_KEY, term: term} , (videos)  => {
			//this.setState({ videos:videos })
			this.setState({ 
				videos:videos,
				selectedVideo: videos[0],
			});
		});
	}

	render () {

		//throttling user input
		const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300);

		return (
			<div>
				<SearchBar onSearchTermChange={ videoSearch } />
				<VideoDetail video={this.state.selectedVideo} />
				<VideoList 
					videos={this.state.videos} 
			 		onVideoSelect={ selectedVideo => this.setState({selectedVideo}) } 
			 	/>
			</div>
		);
	}
}

// Put generated HTML and put into page
ReactDOM.render(<App />, document.querySelector('.container'));